#!/bin/bash

echo "Apply Blog database migrations"
python manage.py migrate

# Start server
echo "Starting Blog server"
/usr/local/bin/gunicorn Blog.wsgi:application -w 2 -b :8000

